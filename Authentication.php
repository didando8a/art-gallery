<?php

class Authentication
{
    const LOGIN_FILE = 'login.php';
    const MAIN_PAGE = 'index.php';
    const MAIN_ADMIN = 'menu.php';

    protected $db;

    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @return bool
     */
    function isAthenticated()
    {
        return isset($_SESSION['auth']) && isset($_SESSION['user']);
    }

    /**
     * @return bool
     */
    public function authenticate()
    {
        if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['username']) && isset($_POST['password'])
        ) {
            $user = $_POST['username'];
            $password = md5($_POST['password']);
            $userData = $this->getUser($user, $password);
            if ($userData) {
                $_SESSION['auth'] = true;
                $_SESSION['user'] = $userData['user'];

                return $userData;
            }
        }
        return false;
    }

    /**
     * @param $user
     * @param $password
     * @return bool
     */
    public function getUser($user, $password)
    {
        $statement =
            $this->db->prepare('SELECT * FROM admin where lower(user) = lower(:user) and password = :password',
                array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY)
            );
        $queryFetch = $statement->execute(array(':user' => $user, ':password' => $password));
        if ($statement->rowCount() > 0) {
            $userData = $statement->fetch(PDO::FETCH_ASSOC);

            return $userData;
        }

        return false;
    }

    /**
     * @return
     */
    public function logout()
    {
        if (isset($_GET['logout']) && $_GET['logout'] == 1) {
            if (isset($_SESSION['auth'])) {
                unset($_SESSION['auth']);
            }
            if (isset($_SESSION['user'])) {
                unset($_SESSION['user']);
            }
        }

        self::redirect(self::MAIN_PAGE);
    }

    /**
     * @param $page
     */
    static public function redirect($page)
    {
        $redirect = sprintf('Location: %s', $page);
        header($redirect);
        die();
    }
}