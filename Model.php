<?php

class Model
{
    protected $db;

    /**
     * @param PDO $db
     */
    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @param $name
     * @param null $description
     * @param null $year
     * @param null $price
     * @param null $artist
     * @return bool
     */
    public function addItem($name, $description = null, $year = null, $price = null, $artist = null)
    {
        $insert = "INSERT INTO items (name, description, artist, year, price) VALUES(:name, :description, :artist, :year, :price)";
        try {
            $statement = $this->db->prepare($insert, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $result = $statement->execute(
                array(
                    ':name' => $name, ':description' => $description, ':year' => $year, ':price' => $price,
                    ':artist' => $artist
                )
            );

            return $result;
        } catch (PDOException $e) {
            echo "Oops, something went wrong!";
            exit();
        }
    }

    /**
     * @param $name
     * @param null $description
     * @param null $year
     * @param null $price
     * @param null $artist
     * @return bool
     */
    public function addImageByItem($itemId)
    {
        try {

            // Undefined | Multiple Files | $_FILES Corruption Attack
            // If this request falls under any of them, treat it invalid.
            if (
                !isset($_FILES['imageToUpload']['error']) ||
                is_array($_FILES['imageToUpload']['error'])
            ) {
                throw new RuntimeException('Invalid parameters.');
            }

            // Check $_FILES['imageToUpload']['error'] value.
            switch ($_FILES['imageToUpload']['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new RuntimeException('Exceeded filesize limit.');
                default:
                    throw new RuntimeException('Unknown errors.');
            }

            // You should also check filesize here.
            if ($_FILES['imageToUpload']['size'] > 1000000) {
                throw new RuntimeException('Exceeded filesize limit.');
            }

            // DO NOT TRUST $_FILES['imageToUpload']['mime'] VALUE !!
            // Check MIME Type by yourself.
            $finfo = new finfo(FILEINFO_MIME_TYPE);
            if (false === $ext = array_search(
                    $finfo->file($_FILES['imageToUpload']['tmp_name']),
                    array(
                        'jpg' => 'image/jpeg',
                        'png' => 'image/png',
                        'gif' => 'image/gif',
                    ),
                    true
                )) {
                throw new RuntimeException('Invalid file format.');
            }

            // You should name it uniquely.
            // DO NOT USE $_FILES['imageToUpload']['name'] WITHOUT ANY VALIDATION !!
            // On this example, obtain safe unique name from its binary data.
            $path = sprintf('./uploads/%s.%s',
                sha1_file($_FILES['imageToUpload']['tmp_name']),
                $ext
            );
            if (!move_uploaded_file(
                $_FILES['imageToUpload']['tmp_name'],
                $path
            )) {
                throw new RuntimeException('Failed to move uploaded file.');
            }

            $insert = "INSERT INTO pictures (path, file_name, item_id) VALUES(:path, :fileName, :itemId)";
            try {
                $statement = $this->db->prepare($insert, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $result = $statement->execute(
                    array(
                        ':path' => $path, ':fileName' => $_FILES['imageToUpload']['name'], ':itemId' => $itemId,
                    )
                );

                return $result;
            } catch (PDOException $e) {
                echo "Oops, something went wrong!";
                exit();
            }
        } catch (RuntimeException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param $name
     * @param null $description
     * @param null $year
     * @param null $price
     * @param null $artist
     * @return bool
     */
    public function updateItem($name, $description = null, $year = null, $price = null, $artist = null, $id)
    {
        $insert = "UPDATE items SET name = :name, description = :description, artist = :artist, year = :year, price = :price WHERE id = :id";
        try {
            $statement = $this->db->prepare($insert, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $result = $statement->execute(
                array(
                    ':name' => $name, ':description' => $description, ':year' => $year, ':price' => $price,
                    ':artist' => $artist, ':id' => $id
                )
            );

            return $result;
        } catch (PDOException $e) {
            echo "Oops, something went wrong!";
            exit();
        }
    }

    /**
     * @return bool
     */
    public function getItems()
    {
        try {
            $select = "SELECT * FROM items";
            $statement = $this->db->prepare($select, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $result = $statement->execute(array());
            if (!$result) {
                return false;
            }
            $items = $statement->fetchAll(PDO::FETCH_ASSOC);
//            var_dump($items);exit();

            return $items;
        } catch (PDOException $e) {
            echo "Oops, something went wrong!";
            exit();
        }
    }

    /**
     * @return bool
     */
    public function getItemsWithMainImage()
    {
        try {
            $select = "SELECT i.*, p.path FROM items as i left join pictures p on(p.item_id = i.id) GROUP BY i.id";
            $statement = $this->db->prepare($select, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $result = $statement->execute(array());
            if (!$result) {
                return false;
            }
            $items = $statement->fetchAll(PDO::FETCH_ASSOC);

            return $items;
        } catch (PDOException $e) {
            echo "Oops, something went wrong!";
            exit();
        }
    }

    /**
     * /**
     * @param $itemId
     * @return bool|mixed
     */
    public function getItem($itemId)
    {
        try {
            $select = "SELECT * FROM items WHERE id = :id";
            $statement = $this->db->prepare($select, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $result = $statement->execute(array(':id' => $itemId));
            if (!$result) {
                return false;
            }

            if ($statement->rowCount() > 0) {
                $itemData = $statement->fetch(PDO::FETCH_ASSOC);

                return $itemData;
            }

            return false;
        } catch (PDOException $e) {
            echo "Oops, something went wrong!";
            exit();
        }
    }


    /**
     * @return bool
     */
    public function getImagesByItem($itemId)
    {
        try {
            $select = "SELECT * FROM pictures WHERE item_id = :itemId";
            $statement = $this->db->prepare($select, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $result = $statement->execute(array(':itemId' => $itemId));
            if (!$result) {
                return false;
            }
            $pictures = $statement->fetchAll(PDO::FETCH_ASSOC);

            return $pictures;
        } catch (PDOException $e) {
            echo "Oops, something went wrong!";
            exit();
        }
    }

    /**
     * @param $itemId
     * @return bool
     */
    public function removeItem($itemId)
    {
        try {
            $remove = "DELETE FROM items WHERE id = :itemId";
            $statement = $this->db->prepare($remove, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $result = $statement->execute(array(':itemId' => $itemId));

            return $result;
        } catch (PDOException $e) {
            echo "Oops, something went wrong!";
            exit();
        }
    }

    /**
     * @param $imageId
     * @return bool|mixed
     * @throws Exception
     */
    public function removeImage($imageId)
    {
        $image = $this->getImage($imageId);
        if (!$image) {
            return false;
        }

        try {
            $remove = "DELETE FROM pictures WHERE id = :imageId";
            $statement = $this->db->prepare($remove, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $result = $statement->execute(array(':imageId' => $imageId));

        } catch (PDOException $e) {
            echo "Oops, something went wrong!";
            exit();
        }

        if ($result && !unlink($image['path'])) {
            $errorMessage = sprintf("The image %d has been delete from the db the could be removed from the server", $image['id']);
            throw new Exception($errorMessage);
        }

        return $image;
    }

    /**
     * /**
     * @param $imageId
     * @return bool|mixed
     */
    public function getImage($imageId)
    {
        try {
            $select = "SELECT * FROM pictures WHERE id = :imageId";
            $statement = $this->db->prepare($select, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $result = $statement->execute(array(':imageId' => $imageId));
            if (!$result) {
                return false;
            }

            if ($statement->rowCount() > 0) {
                $imageData = $statement->fetch(PDO::FETCH_ASSOC);

                return $imageData;
            }

            return false;
        } catch (PDOException $e) {
            echo "Oops, something went wrong!";
            exit();
        }
    }
}