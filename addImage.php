<?php

session_start();
session_regenerate_id(true);

require_once __DIR__ . '/auth.php';
require_once __DIR__ . '/twigBootstrap.php';
require_once __DIR__ . '/Model.php';

$template = $twig->loadTemplate('add_image.html.twig');
$variables = array();

try {
    $db = new PDO('mysql:host=localhost;dbname=art_gallery;charset=utf8', 'root', 'Colombia7');
    $model = new Model($db);
} catch (PDOException $e) {

}

$itemId = null;
if (isset($_GET['itemId'])) {
    $itemId = (is_numeric($_GET['itemId'])) ? intval($_GET['itemId']) : null;
} elseif (isset($_POST['add'])) {
    $itemId  = (is_numeric(key($_POST['add']))) ? key($_POST['add']) : null;
}

if (!$model->getItem($itemId)) {
    $_SESSION['status'] = "danger";
    $_SESSION['message'] = "No item found";
    Authentication::redirect(Authentication::MAIN_ADMIN);
}

$variables['itemId'] = $itemId;


if (isset($_POST['add'])) {
//    var_dump($_FILES);exit();

    $fieldErrors = array();
    $isValid = true;

    if (isset($_POST['year']) && !empty($_POST['year']) && !is_numeric($_POST['year'])) {
        $fieldErrors[] = 'The year field must be a number';
        $isValid = false;
    }

    $variables['fieldErrors'] = $fieldErrors;

    $year = isset($_POST['year']) ? intval($_POST['year']) : null ;
    $price = isset($_POST['price']) ? intval(($_POST['price'] * 100)) : null ;

    if ($isValid) {

        try {
            $added = $model->addImageByItem($itemId);

            if ($added) {
                $_SESSION['status'] = 'success';
                $_SESSION['message'] = 'Image added successfully';

                $imagesUrl = sprintf("imagesAdmin.php?itemId=%d", $itemId);
                Authentication::redirect($imagesUrl);
            }
        } catch (PDOException $e) {

        }
        $message = "Opps, something went wrong!";
        $status = "danger";
        $variables['status'] = $status;
        $variables['message'] = $message;
    }
}

$template->display($variables);