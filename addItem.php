<?php

session_start();
session_regenerate_id(true);

require_once __DIR__ . '/auth.php';
require_once __DIR__ . '/twigBootstrap.php';
require_once __DIR__ . '/Model.php';

$template = $twig->loadTemplate('add_item.html.twig');
$variables = array();

try {
    $db = new PDO('mysql:host=localhost;dbname=art_gallery;charset=utf8', 'root', 'Colombia7');
    $model = new Model($db);
} catch (PDOException $e) {

}

if (isset($_POST['add']) || isset($_POST['update'])) {
    $fieldErrors = array();
    $isValid = true;

    if (isset($_POST['year']) && !empty($_POST['year']) && !is_numeric($_POST['year'])) {
        $fieldErrors[] = 'The year field must be a number';
        $isValid = false;
    }

    if (isset($_POST['price']) && !empty($_POST['price']) && !is_numeric($_POST['price'])) {
        $fieldErrors[] = 'The price field must be a number';
        $isValid = false;
    }

    if (!isset($_POST['name'])) {
        $fieldErrors[] = 'The name field cannot be null';
        $isValid = false;
    }

    $variables['fieldErrors'] = $fieldErrors;

    $name = isset($_POST['name']) ? $_POST['name'] : null ;
    $artist = isset($_POST['artist']) ? $_POST['artist'] : null ;
    $description = isset($_POST['description']) ? $_POST['description'] : null ;
    $year = isset($_POST['year']) ? intval($_POST['year']) : null ;
    $price = isset($_POST['price']) ? intval(($_POST['price'] * 100)) : null ;

    if ($isValid) {

        try {
            $db = new PDO('mysql:host=localhost;dbname=art_gallery;charset=utf8', 'root', 'Colombia7');
            $model = new Model($db);

            if (isset($_POST['update'])) {
                $id = key($_POST['update']);
                $updated = $model->updateItem($name, $description, $year, $price, $artist, $id);

                if ($updated) {
                    $_SESSION['status'] = 'success';
                    $_SESSION['message'] = 'Item updated successfully';
                    Authentication::redirect(Authentication::MAIN_ADMIN);
                }
            } else {
                $added = $model->addItem($name, $description, $year, $price, $artist);

                if ($added) {
                    $_SESSION['status'] = 'success';
                    $_SESSION['message'] = 'Item added successfully';
                    Authentication::redirect(Authentication::MAIN_ADMIN);
                }
            }

        } catch (PDOException $e) {

        }
        $message = "Opps, something went wrong!";
        $status = "danger";
        $variables['status'] = $status;
        $variables['message'] = $message;
    }
}

if (isset($_GET['id']) && is_numeric($_GET['id'])) {
    $item = $model->getItem($_GET['id']);

    if (!$item) {
        $_SESSION['status'] = 'danger';
        $_SESSION['message'] = 'The item you are trying to edit does not exists';
        Authentication::redirect(Authentication::MAIN_ADMIN);
    }

    $variables['item'] = $item;
    $variables['action'] = 'update';
}

$template->display($variables);