<?php

//session_start();
//session_regenerate_id(true);

require_once __DIR__ . '/pdoConnection.php';
require_once __DIR__ . '/Authentication.php';

$authentication = new Authentication($db);

if(!$authentication->isAthenticated()) {
    $authentication->redirect(Authentication::LOGIN_FILE);
}
