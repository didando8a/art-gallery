<?php

session_start();
session_regenerate_id(true);

require_once __DIR__ . '/twigBootstrap.php';
require_once __DIR__ . '/Model.php';

$template = $twig->loadTemplate('detail.html.twig');

$author = "Julius Kato";
$variables = array();
$variables['author'] = $author;

/**
 * Creating sort of FlashBag for status and message
 */
if (isset($_SESSION['status']) && isset($_SESSION['message'])) {
    $variables['status'] = $_SESSION['status'];
    $variables['message'] = $_SESSION['message'];
    unset($_SESSION['status']);
    unset($_SESSION['message']);
}

try {
    $db = new PDO('mysql:host=localhost;dbname=art_gallery;charset=utf8', 'root', 'Colombia7');
} catch (PDOException $e) {
    echo "Opps, something went wrong!";
    exit();
}

$model = new Model($db);

if (isset($_GET['itemId']) && is_numeric($_GET['itemId'])) {
    $item = $model->getItem($_GET['itemId']);
    if (!$item) {
        $_SESSION['status'] = 'danger';
        $_SESSION['message'] = 'Error trying to find the item';
        Authentication::redirect(Authentication::MAIN_PAGE);
    }

    $images = $model->getImagesByItem($item['id']);

    $variables['item'] = $item;
    $variables['images'] = $images;

} else {
    $_SESSION['status'] = 'danger';
    $_SESSION['message'] = 'Item missed';
    Authentication::redirect(Authentication::MAIN_PAGE);
}

$template->display($variables);
