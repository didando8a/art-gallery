<?php

session_start();
session_regenerate_id(true);

require_once __DIR__ . '/auth.php';
require_once __DIR__ . '/twigBootstrap.php';
require_once __DIR__ . '/Model.php';

$variables = array();
//$itemId = (isset($_GET['itemId']) && is_numeric($_GET['itemId'])) ? $_GET['itemId'] : 0;

if (isset($_SESSION['status']) && isset($_SESSION['message'])) {
    $variables['status'] = $_SESSION['status'];
    $variables['message'] = $_SESSION['message'];
    unset($_SESSION['status']);
    unset($_SESSION['message']);
}

$template = $twig->loadTemplate('images_admin.html.twig');

try {
    $db = new PDO('mysql:host=localhost;dbname=art_gallery;charset=utf8', 'root', 'Colombia7');
    $model = new Model($db);
    $itemId = (isset($_GET['itemId']) && is_numeric($_GET['itemId'])) ? $_GET['itemId'] : 0;
    $variables['itemId'] = $itemId;
    if (!$model->getItem($itemId)) {
        $_SESSION['status'] = "danger";
        $_SESSION['message'] = "No item found";
        Authentication::redirect(Authentication::MAIN_ADMIN);
    }
    $images = $model->getImagesByItem($itemId);
    if ($images) {
        $variables['images'] = $images;
    }
} catch (PDOException $e) {

}

$template->display($variables);