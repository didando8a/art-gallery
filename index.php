<?php

session_start();
session_regenerate_id(true);

require_once __DIR__ . '/twigBootstrap.php';
require_once __DIR__ . '/Model.php';

$template = $twig->loadTemplate('index.html.twig');

$author = "Julius Kato";
$variables = array();
$variables['author'] = $author;

/**
 * Creating sort of FlashBag for status and message
 */
if (isset($_SESSION['status']) && isset($_SESSION['message'])) {
    $variables['status'] = $_SESSION['status'];
    $variables['message'] = $_SESSION['message'];
    unset($_SESSION['status']);
    unset($_SESSION['message']);
}

try {
    $db = new PDO('mysql:host=localhost;dbname=art_gallery;charset=utf8', 'root', 'Colombia7');
    $model = new Model($db);
    $items = $model->getItemsWithMainImage();
} catch (PDOException $e) {
    echo "Opps, something went wrong!";
    exit();
}

//$statement = $db->query('SELECT * FROM items');
//$items = $statement->fetchAll(PDO::FETCH_ASSOC);

$variables['items'] = $items;

$template->display($variables);