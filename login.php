<?php

session_start();
session_regenerate_id(true);

require_once __DIR__ . '/twigBootstrap.php';
require_once __DIR__ . '/Authentication.php';
require_once __DIR__ . '/pdoConnection.php';

$template = $twig->loadTemplate('login.html.twig');
$variables = array();

$authentication = new Authentication($db);

if(!$authentication->isAthenticated()) {
    if (isset($_POST['login'])) {
        if ((isset($_POST['username']) && $_POST['username'] === "") || (isset($_POST['password']) && $_POST['password'] === "" )) {
            $variables['authenticationFailed'] = 'Password and user cannot be null';
        } else {
            $user = $authentication->authenticate();
            if (!$user) {
                $variables['authenticationFailed'] = 'The username or the password are incorrect';
            } else {
                Authentication::redirect(Authentication::MAIN_ADMIN);
            }
        }
    }
}

if (isset($_GET['logout'])) {
    $authentication->logout();
}

if($authentication->isAthenticated()) {
    Authentication::redirect(Authentication::MAIN_ADMIN);
}

$template->display($variables);
