<?php

session_start();
session_regenerate_id(true);

require_once __DIR__ . '/auth.php';
require_once __DIR__ . '/twigBootstrap.php';
require_once __DIR__ . '/Model.php';

$variables = array();

if (isset($_SESSION['status']) && isset($_SESSION['message'])) {
    $variables['status'] = $_SESSION['status'];
    $variables['message'] = $_SESSION['message'];
    unset($_SESSION['status']);
    unset($_SESSION['message']);
}

$template = $twig->loadTemplate('menu.html.twig');

try {
    $db = new PDO('mysql:host=localhost;dbname=art_gallery;charset=utf8', 'root', 'Colombia7');
    $model = new Model($db);
    $items = $model->getItems();
    if ($items) {
        $variables['items'] = $items;
    }
} catch (PDOException $e) {

}

$template->display($variables);