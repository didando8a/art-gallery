<?php

session_start();
session_regenerate_id(true);

require_once __DIR__ . '/auth.php';
require_once __DIR__ . '/twigBootstrap.php';
require_once __DIR__ . '/Model.php';

$db = new PDO('mysql:host=localhost;dbname=art_gallery;charset=utf8', 'root', 'Colombia7');
$model = new Model($db);

if (isset($_GET['itemId'])) {
    $isValid = true;

    if (!is_numeric($_GET['imageId'])) {
        $_SESSION['status'] = 'danger';
        $_SESSION['message'] = 'The image id must be a number';
        $isValid = false;
    }

    if ($isValid) {

        try {
            $removed = $model->removeImage($_GET['imageId']);

            if ($removed) {
                $_SESSION['status'] = 'success';
                $_SESSION['message'] = sprintf('image %d removed successfully', $removed['id']);
            }
        } catch (PDOException $e) {
            $_SESSION['status'] = "danger";
            $_SESSION['message'] = "Opps, something went wrong!";
        }

    }
} else {
    $_SESSION['status'] = 'danger';
    $_SESSION['message'] = 'Oops, Something went wrong';
}

$url = Authentication::MAIN_ADMIN;
if (isset($_GET['itemId']) && is_numeric($_GET['itemId'])) {
    if ($model->getItem($_GET['itemId'])) {
        $url = sprintf('imagesAdmin.php?itemId=%d', $_GET['itemId']);
    }
}
Authentication::redirect($url);
