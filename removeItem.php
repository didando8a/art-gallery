<?php

session_start();
session_regenerate_id(true);

require_once __DIR__ . '/auth.php';
require_once __DIR__ . '/twigBootstrap.php';
require_once __DIR__ . '/Model.php';

if (isset($_GET['id'])) {
    $isValid = true;

    if (!is_numeric($_GET['id'])) {
        $_SESSION['status'] = 'danger';
        $_SESSION['message'] = 'The id must be a number';
        $isValid = false;
    }

    if ($isValid) {

        try {
            $db = new PDO('mysql:host=localhost;dbname=art_gallery;charset=utf8', 'root', 'Colombia7');
            $model = new Model($db);
            $removed = $model->removeItem($_GET['id']);

            if ($removed) {
                $_SESSION['status'] = 'success';
                $_SESSION['message'] = 'Item removed successfully';
            }
        } catch (PDOException $e) {
            $_SESSION['status'] = "danger";
            $_SESSION['message'] = "Opps, something went wrong!";
//            echo $e->getMessage();exit();
        }

    }
} else {
    $_SESSION['status'] = 'danger';
    $_SESSION['message'] = 'The id is mandatory';
}

Authentication::redirect(Authentication::MAIN_ADMIN);
